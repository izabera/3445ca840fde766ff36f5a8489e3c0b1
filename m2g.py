#!/usr/bin/env python3
# a basic alternative to make2graph, significantly less painful to read

import re
import sys


class Node:
    def __init__(self, target):
        self.target = target
        self.must_remake = False
        self.deps = set()


def scandeps():
    stack = [("", -1)]
    depsdict = {"": Node("")}

    for line in sys.stdin:
        match = re.match(r"(\s*)([^`']*)[`']([^']*)'", line)
        if not match:
            continue
        indent, text, target = match.groups()
        level = len(indent)

        if text == "Considering target file " or text == "Pruning file ":
            while stack and stack[-1][1] >= level:
                stack.pop()
            parent = stack[-1]
            stack.append((target, level))

            if target not in depsdict:
                depsdict[target] = Node(target)
            depsdict[parent[0]].deps.add(target)

        if text == "Must remake target ":
            depsdict[target].must_remake = True

    return depsdict


def dumpdot(depgraph):
    print("digraph G {")

    for name, node in depgraph.items():
        if name == "":
            continue
        print(f'"{name}" [color="{"red" if node.must_remake else "forestgreen"}"]')
        for dep in node.deps:
            print(f'"{dep}" -> "{name}"')

    print("}")


# make2graph doesn't ignore these but they always look dumb in the graph
while line := input():
    if line == "Updating goal targets....":
        break

dumpdot(scandeps())